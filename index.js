import 'dotenv/config';
import { ListObjectsCommand, S3Client } from "@aws-sdk/client-s3";
import express from 'express';
import Router from 'express-promise-router';

const app = express();
const port = process.env.NODE_PORT || 5000;

const s3 = new S3Client({
  region: 'eu-central-1'
})

const router = Router();

router.get('/list', async (req, res) => {
  const response = await s3.send(new ListObjectsCommand({
    Bucket: 'xw-cloudtask-schroeter-simon',
  }));
  res.json({ contents: response.Contents });
});
router.get('/', async (req, res) => {
  res.end('ok');
})

app.use(router).listen(port, () => {
  console.log(`Listening on port ${port}`)
})